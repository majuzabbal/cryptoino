# Cryptoino

Descrição: Jogo sobre criptografia prgramado em C, ideal para o Arduino.

- Jogo com fases;
- Fase mais fácil é uma criptografia siméétrica de chave pública;
- Fase média com fase privada, dicas serãão dadas para que o jogador descubra a fase;
- Fase mais dificil é feita com criptografia RSA;
- Entrada de dados, jogados tem que decodificar o código gerado aleatoriamente;
- Matriz de led para saída de dados.

Melhoramentos futuros: Criação de imagens na matriz 8x8, esconder mensagem na imagem.